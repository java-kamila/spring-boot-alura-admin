# Spring Boot Admin Server

Projeto para monitoração de APIs Spring Boot. Segundo [Spring-Boot-Admin](https://github.com/codecentric/spring-boot-admin).
Dependências necessárias:
```
	<dependency>
	    <groupId>de.codecentric</groupId>
	    <artifactId>spring-boot-admin-starter-server</artifactId>
	    <version>2.1.6</version>
	</dependency>
	<dependency>
	    <groupId>org.springframework.boot</groupId>
	    <artifactId>spring-boot-starter-web</artifactId>
	</dependency>
```

Classe *SpringBootAdminApplication*:
```
@Configuration
@EnableAutoConfiguration
@EnableAdminServer
public class SpringBootAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootAdminApplication.class, args);
    }
}
```

#### Spring Boot Admin Client
API que será monitorada.

###### Monitoramento com Spring Boot Actuator
 - Para adicionar o Spring Boot Actuator no projeto, devemos adicioná-lo como uma dependência no arquivo pom.xml;
 - Para acessar as informações disponibilizadas pelo Actuator, devemos entrar no endereço http://localhost:8080/actuator;
 - Para liberar acesso ao **Actuator** no **Spring Security**, devemos chamar o método `.antMatchers(HttpMethod.GET, "/actuator/**")`;
 - Para que o Actuator exponha mais informações sobre a API, devemos adicionar as propriedades `management.endpoint.health.show-details=always` e `management.endpoints.web.exposure.include=*` no arquivo application.properties;
 - Para o **Spring Boot Admin** conseguir monitorar outra API, devemos adicionar no projeto da API o módulo spring-boot-admin-client e também adicionar a propriedade `spring.boot.admin.client.url=http://localhost:8081` no arquivo **application.properties**;
 
#### Spring Boot Admin Server
 - Para utilizar o Spring Boot Admin, devemos criar um projeto Spring Boot e adicionar nele os módulos spring-boot-starter-web e spring-boot-admin-server;
 - Para trocar a porta na qual o servidor do Spring Boot Admin rodará, devemos adicionar a propriedade server.port=8081 no arquivo application.properties;
 - Para acessar a interface gráfica do Spring Boot Admin, devemos entrar no endereço deste projeto `http://localhost:8081`.  